public class Main {

    public static void main(String[] args) {
        new Main();
    }
    
    public Main() {
        PositionWA p = new PositionWA();

        p.purchase(new Transact("0.00001310", 626));
        p.purchase(new Transact("0.00001269", 626));
        p.purchase(new Transact("0.00001270", 1252));
        p.purchase(new Transact("0.00001243", 2504));
        p.purchase(new Transact("0.00001205", 1036));
        p.purchase(new Transact("0.00001219", 2774));
        p.purchase(new Transact("0.00001212", 1198));
        p.purchase(new Transact("0.00001192", 1807));
        p.purchase(new Transact("0.00001193", 6033));
        p.purchase(new Transact("0.00001191", 2176));
        p.purchase(new Transact("0.00001118", 20032));
        p.purchase(new Transact("0.00001085", 40064));
        
        
        System.out.println();

        //p.sell(new Transact("0.00001147", 32355));
        //p.sell(new Transact("0.00001147", 8501));
        //p.sell(new Transact("0.00001149", 39272));
        
        p.sell(new Transact(p.priceAveragePrice().toString(), 3412));
        p.sell(new Transact(p.priceAveragePrice().toString(), 7059));
        p.sell(new Transact(p.priceAveragePrice().toString(), 16617));
        p.sell(new Transact(p.priceAveragePrice().toString(), 5267));
        
        p.sell(new Transact(p.priceAveragePrice().toString(), 8501));
        p.sell(new Transact(p.priceAveragePrice().toString(), 23520));
        p.sell(new Transact(p.priceAveragePrice().toString(), 3393));
        p.sell(new Transact(p.priceAveragePrice().toString(), 3951));
        p.sell(new Transact(p.priceAveragePrice().toString(), 590));
        p.sell(new Transact(p.priceAveragePrice().toString(), 4147));
        p.sell(new Transact(p.priceAveragePrice().toString(), 1816));
        p.sell(new Transact(p.priceAveragePrice().toString(), 1855));
        
    }

}
