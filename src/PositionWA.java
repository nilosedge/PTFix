import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import lombok.Getter;

@Getter
public class PositionWA {

    private MathContext mc = new MathContext(8, RoundingMode.HALF_UP);

    BigDecimal positionValue = BigDecimal.ZERO;
    BigDecimal positionAmount = BigDecimal.ZERO;
    
    public void purchase(Transact t) {
        positionValue = positionValue.add(t.getPrice().multiply(t.getAmount()));
        positionAmount = positionAmount.add(t.getAmount());
        System.out.println("Position: Amount: " + positionAmount + " Value: " + positionValue);
        priceAveragePrice();
    }
    
    public BigDecimal priceAveragePrice() {
        if(positionAmount.compareTo(BigDecimal.ZERO) != 0) {
            System.out.println("AverageCost: " + positionValue.divide(positionAmount, mc));
            return positionValue.divide(positionAmount, mc);
        } else {
            System.out.println("AverageCost: " + BigDecimal.ZERO);
            return BigDecimal.ZERO;
        }
    }
    
    public void sell(Transact t) {
        positionValue = positionValue.subtract(t.getPrice().multiply(t.getAmount()));
        positionAmount = positionAmount.subtract(t.getAmount());
        if(positionAmount.equals(BigDecimal.ZERO)) positionValue = BigDecimal.ZERO;
        System.out.println("Position: Amount: " + positionAmount + " Value: " + positionValue);
    }

}
