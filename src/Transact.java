import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter @ToString
public class Transact {

    private BigDecimal price = BigDecimal.ZERO;
    private BigDecimal amount = BigDecimal.ZERO;
    
    public Transact(String price, int amount) {
        this.price = new BigDecimal(price);
        this.amount = new BigDecimal(amount);
    }

}
