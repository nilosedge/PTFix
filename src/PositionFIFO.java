import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class PositionFIFO {

    private MathContext mc = new MathContext(8, RoundingMode.HALF_UP);

    private List<Transact> transactions = new ArrayList<>();
    
    public void purchase(Transact transact) {
        transactions.add(transact);
        priceAveragePrice(transactions);
    }
    
    public void priceAveragePrice(List<Transact> trans) {
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal sum = BigDecimal.ZERO;
        
        for(Transact t: trans) {
            sum = sum.add(t.getAmount().multiply(t.getPrice()));
            totalAmount = totalAmount.add(t.getAmount());
        }

        if(sum.compareTo(BigDecimal.ZERO) != 0) {
            System.out.println("AverageCost: " + sum.divide(totalAmount, mc));
        }
    }
    
    public List<Transact> sell(Transact transact) {
        System.out.println("Selling: " + transact.getAmount());
        ArrayList<Transact> sellList = new ArrayList<>();
        ArrayList<Transact> newList = new ArrayList<>();
        
        BigDecimal amount = transact.getAmount();

        for(Transact t: transactions) {
            if(t.getAmount().compareTo(amount) >= 0) {
                if(amount.compareTo(BigDecimal.ZERO) > 0) {
                    Transact trans = new Transact(t.getPrice().toString(), amount.intValue());
                    sellList.add(trans);
                    BigDecimal prev = amount;
                    amount = amount.subtract(t.getAmount());
                    t.setAmount(t.getAmount().subtract(prev));
                }
                newList.add(t);
            } else {
                sellList.add(t);
                amount = amount.subtract(t.getAmount());
            }
        }
        
        transactions = newList;

        System.out.println("Sell: " + sellList);
        priceAveragePrice(sellList);
        return sellList;
    }

}
